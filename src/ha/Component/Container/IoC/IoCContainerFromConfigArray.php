<?php
declare(strict_types = 1);

namespace ha\Component\Container\IoC;


use ha\Component\Configuration\Simple\ConfigurationFromArray;

class IoCContainerFromConfigArray extends IoCContainerDefaultAbstract
{

    public function __construct(array $configuration) {
        foreach ($configuration AS $moduleClass => $moduleConfiguration) {
            if (!isSet($moduleConfiguration[0]) || !is_string($moduleConfiguration[0]) || !class_exists($moduleConfiguration[0])) {
                throw new \Error('Error in configuration');
            }
            if (!isSet($moduleConfiguration[1]) || !is_array($moduleConfiguration[1])) {
                throw new \Error('Error in configuration');
            }
            $moduleClass = $moduleConfiguration[0];
            $moduleConfiguration = new ConfigurationFromArray($moduleConfiguration[1], "{$moduleClass}.cfg");
            $reflection = new \ReflectionClass($moduleClass);
            $moduleName = $moduleConfiguration->get('name');
            $this->$moduleName = $reflection->newInstanceArgs([$moduleConfiguration]);
        }
        parent::__construct($configuration);
    }

}