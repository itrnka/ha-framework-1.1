<?php
declare(strict_types = 1);

namespace ha\Component\Dimension;


class ByteValue
{

    const UNIT_BYTE = 'B';
    const UNIT_KILOBYTE = 'kB';
    const UNIT_MEGABYTE = 'MB';
    const UNIT_GIGABYTE = 'GB';
    const UNIT_TERABYTE = 'TB';

    /** @var float Value in bytes */
    private $value;

    public function __construct(float $value = 0.0, string $unit = self::UNIT_BYTE)
    {
        $this->setValue($value, $unit);
    }

    public function setValue(float $value, string $unit) : void
    {
        switch ($unit) {
            case self::UNIT_BYTE:
                $this->value = $value;
                break;
            case self::UNIT_KILOBYTE:
                $this->value = $value * 1024;
                break;
            case self::UNIT_MEGABYTE:
                $this->value = $value * 1024 * 1024;
                break;
            case self::UNIT_GIGABYTE:
                $this->value = $value * 1024 * 1024 * 1024;
                break;
            case self::UNIT_TERABYTE:
                $this->value = $value * 1024 * 1024 * 1024 * 1024;
                break;
            default:
                throw new \InvalidArgumentException('unit@' . __METHOD__);
        }
    }


    public function getInBytes() : float
    {
        return $this->value;
    }

    public function getInKilobytes() : float
    {
        return $this->value / 1024;
    }

    public function getInMegabytes() : float
    {
        return $this->value / 1024 / 1024;
    }

    public function getInGigabytes() : float
    {
        return $this->value / 1024 / 1024 / 1024;
    }

    public function getInTerabytes() : float
    {
        return $this->value / 1024 / 1024 / 1024 / 1024;
    }

    /**
     * Get as string.
     *
     * @return string
     */
    public function __toString() : string
    {
        return $this->value . ' ' . self::UNIT_BYTE;
    }

    /**
     * Get as string.
     *
     * @return string
     */
    public function __invoke() : string
    {
        return strval($this);
    }

}