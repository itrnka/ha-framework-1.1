<?php
declare(strict_types = 1);

namespace ha\Component\Dimension;


class LengthMetricValue
{

    const UNIT_KILOMETER = 'km';
    const UNIT_METER = 'm';
    const UNIT_DECIMETER = 'dm';
    const UNIT_CENTIMETER = 'cm';
    const UNIT_MILIMETER = 'mm';
    #const UNIT_MICROMETER = 'μm';

    /** @var float Value in meters */
    private $value;

    /**
     * LengthValue constructor.
     *
     * @param float $value
     * @param string $unit
     */
    public function __construct(float $value = 0.0, string $unit = self::UNIT_METER)
    {
        $this->setValue($value, $unit);
    }

    /**
     * Set value
     *
     * @param float $value
     * @param string $unit
     */
    public function setValue(float $value, string $unit) : void
    {
        switch ($unit) {
            case self::UNIT_MILIMETER:
                $this->value = $value / 1000;
                break;
            case self::UNIT_CENTIMETER:
                $this->value = $value / 100;
                break;
            case self::UNIT_DECIMETER:
                $this->value = $value / 10;
                break;
            case self::UNIT_METER:
                $this->value = $value;
                break;
            case self::UNIT_KILOMETER:
                $this->value = $value * 1000;
                break;
            default:
                throw new \InvalidArgumentException('unit@' . __METHOD__);
        }
    }

    /**
     * @return float
     */
    public function getInKilometers() : float
    {
        return $this->value / 1000;
    }

    /**
     * @return float
     */
    public function getInMeters() : float
    {
        return $this->value;
    }

    /**
     * @return float
     */
    public function getInDecimeters() : float
    {
        return $this->value * 10;
    }

    /**
     * @return float
     */
    public function getInCentimeters() : float
    {
        return $this->value * 100;
    }

    /**
     * @return float
     */
    public function getInMilimeters() : float
    {
        return $this->value * 1000;
    }

#    /**
#     * @return float
#     */
#    public function getInInch() : float
#    {
#        return $this->value / 0.0254;
#    }

    /**
     * Get as string.
     *
     * @return string
     */
    public function __toString() : string
    {
        return $this->value . ' ' . self::UNIT_METER;
    }

    /**
     * Get as string.
     *
     * @return string
     */
    public function __invoke() : string
    {
        return strval($this);
    }
}