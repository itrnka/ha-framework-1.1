<?php
declare(strict_types = 1);

namespace ha\App\Builder;


use ha\App\App;
use ha\App\AppDefault;
use ha\Component\Configuration\Configuration;
use ha\Component\Container\IoC\IoCContainerFromConfigArray;


/**
 * Example of default App instance builder. You can use your custom implementation.
 *
 * @package ha\App
 */
class AppBuilderDefault implements AppBuilder
{

    /** @var Configuration $cfg */
    private $cfg;

    /**
     * AppBuilderDefault constructor.
     *
     * @param Configuration $cfg
     */
    public function __construct(Configuration $cfg)
    {
        $this->cfg = $cfg;
    }

    /**
     * Function buildApp
     *
     * @param string $enviromentName
     *
     * @return App
     */
    public function buildApp(string $enviromentName) : App
    {
        // create Logger
        $logger = $this->cfg->get('app.logger.className', true);
        $logger = new $logger($this->cfg);

        // create Dumper
        $dumper = $this->cfg->get('app.dumper.className', true);
        $dumper = new $dumper($this->cfg);

        // create IoC container with middleware instances
        $middleware = new IoCContainerFromConfigArray($this->cfg->get('middleware'));

        // create IoC container with modules
        $modules = new IoCContainerFromConfigArray($this->cfg->get('modules'));

        // build and return app
        $app = new AppDefault($enviromentName, $logger, $dumper, $this->cfg, $modules, $middleware);
        return $app;
    }
}