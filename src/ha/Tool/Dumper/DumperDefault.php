<?php
declare(strict_types = 1);

namespace ha\Tool\Dumper;
use ha\Component\Configuration\Configuration;


/**
 * Default dumper implementation.
 *
 */
class DumperDefault implements Dumper
{

    /**
     * DumperDefault constructor.
     *
     * @param \ha\Component\Configuration\Configuration $configuration Configuration data
     */
    public function __construct(Configuration $configuration)
    {

    }

    /**
     * Dump variables passed as arguments.
     *
     * @return void
     */
    public function dump() : void
    {
        echo PHP_EOL;
        for ($i = 0; $i < func_num_args(); $i++) {
            call_user_func('var_dump', func_get_arg($i));
            echo PHP_EOL;
        }
    }

}