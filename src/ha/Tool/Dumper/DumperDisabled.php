<?php
declare(strict_types = 1);


namespace ha\Tool\Dumper;


use ha\Component\Configuration\Configuration;

/**
 * Class DumperDisabled.
 * Useful for production enviroment, dump() function produces nothing.
 *
 */
class DumperDisabled implements Dumper
{

    /**
     * DumperDisabled constructor.
     *
     * @param \ha\Component\Configuration\Configuration $configuration Configuration data
     */
    public function __construct(Configuration $configuration)
    {

    }

    /**
     * Dump variables passed as arguments.
     *
     * @return void
     */
    public function dump() : void
    {
        // nothing, output disabled
    }
}