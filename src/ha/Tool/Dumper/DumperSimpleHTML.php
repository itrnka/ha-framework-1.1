<?php
declare(strict_types = 1);


namespace ha\Tool\Dumper;


use ha\Component\Configuration\Configuration;

class DumperSimpleHTML implements Dumper
{

    /**
     * DumperSimpleHTML constructor.
     *
     * @param \ha\Component\Configuration\Configuration $configuration Configuration data
     */
    public function __construct(Configuration $configuration)
    {

    }

    /**
     * Dump variables passed as arguments.
     *
     * @return void
     */
    public function dump() : void
    {
        echo PHP_EOL . "<pre style=\"display:block; border: 2px solid red; color: black; background: #ffff99; padding: 1em;\">DUMP:" . PHP_EOL;
        for ($i = 0; $i < func_num_args(); $i++) {
            call_user_func('var_dump', func_get_arg($i));
        }
        echo "</pre>" . PHP_EOL;
    }

}