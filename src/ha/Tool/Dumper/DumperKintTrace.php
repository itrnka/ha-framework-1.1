<?php
declare(strict_types = 1);


namespace ha\Tool\Dumper;
use ha\Component\Configuration\Configuration;


/**
 * Class DumperKint.
 * Use Kint as dump engine.
 *
 * @package ha\Tool\Dumper
 */
class DumperKintTrace implements Dumper
{

    /**
     * DumperKint constructor.
     *
     * @param \ha\Component\Configuration\Configuration $configuration Configuration data
     *
     * @throws \ErrorException
     */
    public function __construct(Configuration $configuration)
    {
        if (!class_exists('\Kint')) {
            throw new \ErrorException(__CLASS__ . ' can not be used because Kint is not installed');
        }
    }

    /**
     * Dump variables passed as arguments.
     *
     * @return void
     */
    public function dump() : void
    {
        $bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 3);
        if (isSet($bt[2])) {
            unset($bt[0], $bt[1]);
            call_user_func_array(['Kint', 'trace'], [$bt]);
        }
    }

}