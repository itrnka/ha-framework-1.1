<?php
declare(strict_types = 1);

namespace ha\Tool\Dumper;
use ha\Component\Configuration\Configuration;


/**
 * Interface for creating instances with var_dump functionality.
 *
 * @package ha\Tool\Dumper
 */
interface Dumper
{


    /**
     * Dumper constructor.
     *
     * @param \ha\Component\Configuration\Configuration $configuration Configuration data
     */
    public function __construct(Configuration $configuration);

    /**
     * Dump variables passed as arguments.
     *
     * @return void
     */
    public function dump() : void;

}