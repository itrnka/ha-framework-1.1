<?php
declare(strict_types = 1);

namespace ha\Tool\Logger;
use ha\Component\Configuration\Configuration;


/**
 * Interface Logger.
 * This is an interface for logging.
 * @package ha\Tool\Logger
 */
interface Logger
{

    /** Trace level. */
    const LEVEL_TRACE = 1;

    /** Trace level. */
    const LEVEL_DEBUG = 2;

    /** Trace level. */
    const LEVEL_INFO  = 3;

    /** Trace level. */
    const LEVEL_WARN  = 4;

    /** Trace level. */
    const LEVEL_ERROR = 5;

    /** Trace level. */
    const LEVEL_FATAL = 6;


    /**
     * Logger constructor.
     *
     * @param \ha\Component\Configuration\Configuration $configuration Configuration data
     */
    public function __construct(Configuration $configuration);

    /**
     * Function log
     *
     * @param int $level
     * @param string $message
     * @param string $group
     *
     * @return void
     */
    public function log(int $level, string $message, string $group = null) : void;

    /**
     * Function trace
     *
     * @param string $message
     * @param string $group
     *
     * @return void
     */
    public function trace(string $message, string $group = null) : void;

    /**
     * Function debug
     *
     * @param string $message
     * @param string $group
     *
     * @return void
     */
    public function debug(string $message, string $group = null) : void;

    /**
     * Function info
     *
     * @param string $message
     * @param string $group
     *
     * @return void
     */
    public function info(string $message, string $group = null) : void;

    /**
     * Function warn
     *
     * @param string $message
     * @param string $group
     *
     * @return void
     */
    public function warn(string $message, string $group = null) : void;

    /**
     * Function error
     *
     * @param string $message
     * @param string $group
     *
     * @return void
     */
    public function error(string $message, string $group = null) : void;

    /**
     * Function fatal
     *
     * @param string $message
     * @param string $group
     *
     * @return void
     */
    public function fatal(string $message, string $group = null) : void;

}