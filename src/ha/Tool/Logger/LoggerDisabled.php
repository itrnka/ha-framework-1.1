<?php
declare(strict_types = 1);

namespace ha\Tool\Logger;
use ha\Component\Configuration\Configuration;


/**
 * Logger implementation with disabled output.
 * Useful for disabling logging (in dev mode, etc.).
 *
 * @package ha\Tool\Logger
 */
class LoggerDisabled implements Logger
{

    public function __construct(Configuration $configuration)
    {

    }

    public function log(int $level, string $message, string $group = null) : void
    {
        // nothing, logging disabled
    }

    public function trace(string $message, string $group = null) : void
    {
        $this->log(Logger::LEVEL_TRACE, $message);
    }

    public function debug(string $message, string $group = null) : void
    {
        $this->log(Logger::LEVEL_DEBUG, $message);
    }

    public function info(string $message, string $group = null) : void
    {
        $this->log(Logger::LEVEL_INFO, $message);
    }

    public function warn(string $message, string $group = null) : void
    {
        $this->log(Logger::LEVEL_WARN, $message);
    }

    public function error(string $message, string $group = null) : void
    {
        $this->log(Logger::LEVEL_ERROR, $message);
    }

    public function fatal(string $message, string $group = null) : void
    {
        $this->log(Logger::LEVEL_FATAL, $message);
    }


}